<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\ConfirmPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\DB;
use App\Http\Controllers\CustomAuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return view('welcome');
//});
Route::get('/',[StudentController::class,'index'])->name('home1');
Route::get('/edit/{id}',[StudentController::class,'edit']);
Route::get('/show/{id}',[StudentController::class,'show']);
Route::get('/student',[StudentController::class,'create']);
Route::post('/store1',[StudentController::class,'store']);
Route::post('/update/{id}',[StudentController::class,'update']);

Route::get('/edit2/{id}',[CourseController::class,'edit']);
Route::get('/show2/{id}',[CourseController::class,'show']);
Route::get('/course',[CourseController::class,'create']);
Route::post('/store',[CourseController::class,'store']);
Route::post('/update2/{id}',[CourseController::class,'update']);
Route::get('/home', [PostController::class, 'index'])->name('home');
Route::get('post/create', [PostController::class, 'create'])->name('post.create');
Route::post('post', [PostController::class, 'store']);
Route::get('post/{id}/edit', [PostController::class, 'edit'])->name('post.edit');
Route::get('post/{id}', [PostController::class, 'show'])->name('post.show');
Route::put('post/{id}', [PostController::class, 'update'])->name('post.update');
Route::delete('post/{id}', [PostController::class, 'destroy'])->name('post.destroy');
Route::get('dashboard', [CustomAuthController::class, 'dashboard']);
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom');
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom');
Route::get('signout', [CustomAuthController::class, 'signout'])->name('signout');
//Route::post('/comment/store', [CommentController::class,'store'])->name('comment.add');
//Route::get('/comment/', function(){
  //  return view('post.single');
//});

//Route::post('/reply/store', [CommentController::class,'replyStore'])->name('reply.add');

//Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Auth::routes();

//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
