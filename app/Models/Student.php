<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'firstName',
        'secondName',
        'age',
        'speciality',
        'image',
        'user_id',



    ];
    protected $casts = [
  'course_id'=> 'array',
    ];
    //protected $table="students";

    public function courses()
    {
        return $this->belongsToMany(course::class,'student_course','student_id','course_id');
    }
    public function posts()
    {
        return $this->belongsTo(Post::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
