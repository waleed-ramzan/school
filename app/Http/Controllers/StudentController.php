<?php

namespace App\Http\Controllers;

use App\models\Student;
use App\models\course;
use App\Models\User;
use App\models\student_course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\File;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
     $courses = course::all();
     $students=Student::all();


        // $students = FacadesDB::table('students')
       // ->join('courses','students.id',"=",'courses.id')
//->select('students.*','students.student','courses.id')
       // ->get();
      //  return view('student',['students'=>$students,'layout'=>'index'],compact($students));
       // return view('student',compact('students'));
//$students = DB::table('courses')
//->join('students', 'courses.id', '=', 'students.id')

//->select('courses.*')
//->get();
//  return dd($products);
return view('student',['students'=>$students,'layout'=>'index'],compact($students));

return view('student',['courses'=>$courses,'layout'=>'index'],compact($courses));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $courses = course::all() ;
      $students = Student::all() ;
      return view('student',['courses'=>$courses,'students'=>$students,'layout'=>'create']);
     // return view('student',['students'=>$students,'layout'=>'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        if($request->hasFile('image')){
            $image=$request->file('image');
            $reImage=time().'.'.$image->getClientOriginalExtension();
            $dest=public_path('/img');
            $image->move($dest,$reImage);

            $data['image'] = 'img/'.$reImage;

        }

        $student = new Student();
$user= User::find(Auth()->user()->id);
       // $course =new course();
        //$course= $request->input('id');
       // $students = $request->input('students', []);
        $student->id = $request->input('id') ;
        $student->firstName = $request->input('firstName') ;
        $student->secondName = $request->input('secondName') ;
        $student->age = $request->input('age') ;
        $student->speciality = $request->input('speciality') ;
        $student->user_id = $user->id;
        $student->image=$reImage;


        $student->save() ;

      //  for ($student=0; $student < count($students); $student++) {
        //    if ($students[$student] != '') {
          //      $students->courses()->attach($students[$student]);
          //  }
        //}
        $student =Student::find(2);
        $student->courses()->attach(2);
        return redirect('/student') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        $students = Student::all();
        return view('student',['students'=>$students,'student'=>$student,'layout'=>'show']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $student = Student::find($id);
      $students = Student::all() ;
      return view('student',['students'=>$students,'student'=>$student,'layout'=>'edit']);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($request->hasFile('image')){
            $image=$request->file('image');
            $reImage=time().'.'.$image->getClientOriginalExtension();
            $dest=public_path('/img');
            $image->move($dest,$reImage);

            $data['image'] = 'img/'.$reImage;

        }
      $student = Student::find($id);
      $student->id = $request->input('id') ;
      $student->firstName = $request->input('firstName') ;
      $student->secondName = $request->input('secondName') ;
      $student->age = $request->input('age') ;
      $student->speciality = $request->input('speciality') ;
      $student->image=$reImage;

      $student->save() ;
      return redirect('/student') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $student = Student::find($id);
      $student->delete() ;
      return redirect('/student') ;
    }
    public function admin()
    {
        return view('admin');
    }
}
